CREATE TABLE Teams (
    id_team SERIAL PRIMARY KEY,
    name_team VARCHAR(255) NOT NULL,
    city_team VARCHAR(255) NOT NULL
);

CREATE TABLE Players (
    id_player SERIAL PRIMARY KEY,
    first_name VARCHAR(255),
    last_name VARCHAR(255),
    nick_name VARCHAR(255),
    date_birth DATE,
    id_team INT NOT NULL REFERENCES Teams(id_team),
);

CREATE TABLE Matches (
    id_match SERIAL PRIMARY KEY,
    date_match DATE NOT NULL,
    id_team1 INT REFERENCES Teams(id_team),
    id_team2 INT REFERENCES Teams(id_team),
    team1_goals INT DEFAULT 0,
    team2_goals INT DEFAULT 0,
    time_match INT DEFAULT 90 NOT NULL,
    extratime_match INT
);

CREATE TABLE Played_time (
    id_sub SERIAL PRIMARY KEY,
    minute_sub INT,
    id_match INT REFERENCES Matches(id_match),
    id_player1 INT DEFAULT 90 NOT NULL REFERENCES Players(id_player),
    id_player2 INT REFERENCES Players(id_player)
);

CREATE TABLE Goals (
    id_goal SERIAL PRIMARY KEY,
    goal_minute INT NOT NULL,
    id_match INT REFERENCES Matches(id_match),
    id_player INT REFERENCES Players(id_player)
);

CREATE TABLE Assists (
    id_assist SERIAL PRIMARY KEY,
    assist_minute INT NOT NULL,
    id_match INT REFERENCES Matches(id_match),
    id_player INT REFERENCES Players(id_player)
);

CREATE TABLE Cards (
    id_card SERIAL PRIMARY KEY,
    card_minute INT NOT NULL,
    card_type BOOLEAN NOT NULL,
    id_match INT REFERENCES Matches(id_match),
    id_player INT REFERENCES Players(id_player)
);

-- команда для psql для створення таблиць в БД
-- psql -U ваш_користувач -d ваша_база_даних -f create_tables.sql
-- якщо БД запаролена попросить ввести пароль