def champ(teams: list, type_champ: int) -> list:
    """
    Функція генерації комбінацій команд для турніру з коловою системою.

    :param teams: Список команд для турніру.
    :param type_champ: Тип турніру (1 коло або 2 кола).

    :return: Список кортежів з парами команд.
    """
    result_tuples = []

    if type_champ == 2:
        for element1 in teams:
            for element2 in teams:
                if element1 != element2:
                    result_tuples.append((element1, element2))
        print(result_tuples, len(result_tuples))
        return result_tuples
    elif type_champ == 1:
        for element1 in range(len(teams)):
            for element2 in range(element1+1, len(teams)):
                result_tuples.append((teams[element1], teams[element2]))
        print(result_tuples, len(result_tuples))
        return result_tuples


team_list = ["a", "b", "c", "d", "e", "f", "g", "i", "k"]
champ(team_list, 1)
