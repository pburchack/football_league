# forms.py у вашому додатку "tournaments"
from django import forms
from .models import Team, Player, Tournament


class PlayerForm(forms.ModelForm):
    class Meta:
        model = Player
        fields = ["name", "surname", "date_of_birth", "is_active"]


