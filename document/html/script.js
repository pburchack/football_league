function saveToJson() {
    var competitionForm = document.getElementById('competition-form');
    var generalForm = document.getElementById('general-form');

    // Отримання даних з форм
    var competitionFormData = new FormData(competitionForm);
    var generalFormData = new FormData(generalForm);

    // Створення об'єкта JSON
    var jsonData = {
        competition: {
            type: competitionFormData.get('competition-type')
        },
        general: {
            leagueName: generalFormData.get('league-name'),
            startDate: generalFormData.get('start-date'),
            endDate: generalFormData.get('end-date')
        }
    };

    // Перетворення в JSON та збереження у файл (це виклик на локальному сервері)
    var jsonString = JSON.stringify(jsonData);
    var blob = new Blob([jsonString], { type: 'application/json' });
    var url = URL.createObjectURL(blob);

    var a = document.createElement('a');
    a.href = url;
    a.download = 'data.json';
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
    URL.revokeObjectURL(url);
}
