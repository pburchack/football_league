from django.db import models


class Player(models.Model):
    name = models.CharField(max_length=100)
    surname = models.CharField(max_length=100)
    date_of_birth = models.DateField()
    is_active = models.BooleanField(default=True)

    def __str__(self) -> str:
        return f"{self.name} {self.surname}"


class Team(models.Model):
    name = models.CharField(max_length=100)
    city = models.CharField(max_length=100)
    is_active = models.BooleanField(default=True)
    players = models.ManyToManyField(Player, blank=True)

    def __str__(self) -> str:
        return f"{self.name} ({self.city})"


class Tournament(models.Model):
    TOURNAMENT_TYPE_CHOICES = [
        ("cup", "Кубок"),
        ("championship", "Чемпіонат"),
    ]

    name = models.CharField(max_length=100)
    tournament_type = models.CharField(max_length=50, choices=TOURNAMENT_TYPE_CHOICES)
    teams = models.ManyToManyField(Team)
    is_active = models.BooleanField(default=True)

    def __str__(self) -> str:
        return f"{self.name} {self.tournament_type}"


class Match(models.Model):
    tournament = models.ForeignKey(Tournament, on_delete=models.CASCADE)
    team1 = models.ForeignKey(Team, on_delete=models.CASCADE, related_name="team1")
    team2 = models.ForeignKey(Team, on_delete=models.CASCADE, related_name="team2")
    result_team1 = models.IntegerField(default=0)
    result_team2 = models.IntegerField(default=0)

    def __str__(self) -> str:
        return f"{self.team1} {self.result_team1} - {self.result_team2} {self.team2}"


class PlayerEvent(models.Model):
    EVENT_TYPE_CHOICES = [
        ("goal", "Гол"),
        ("penalty_goal", "Гол з пенальті"),
        ("assist", "Передача"),
        ("yellow_card", "Жовта картка"),
        ("red_card", "Червона картка"),
        ("autogoal", "Автогол"),
    ]

    player = models.ForeignKey(Player, on_delete=models.CASCADE)
    match = models.ForeignKey(Match, on_delete=models.CASCADE)
    event_type = models.CharField(max_length=50, choices=EVENT_TYPE_CHOICES)
    minute = models.IntegerField()

    def __str__(self) -> str:
        return f"{self.player} ({self.match}) {self.event_type} ({self.minute})"


class PlayerStats(models.Model):
    player = models.ForeignKey(Player, on_delete=models.CASCADE)
    match = models.ForeignKey(Match, on_delete=models.CASCADE)
    total_minutes_played = models.IntegerField(default=90)

    def __str__(self) -> str:
        return f"{self.player} ({self.match})"
