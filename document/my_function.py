from models import PlayerEvent


def events_in_match(match):
    """
    Перевірка подій в матчі

    :param match: Функція приймає об'єкт моделі Match,
    для якого потрібно отримати події.
    Наприклад: match = Match.objects.get(pk=1)

    :return: Повертає словник з подіями, які складаються з списків словників з
    іменами гравців і хвилинами на якій відбулася подія, якщо подія не
    відбулася порожній список не передається в словник.
    """
    event_types = {}

    for event_type, event_type_label in PlayerEvent.EVENT_TYPE_CHOICES:
        events = PlayerEvent.objects.filter(match=match, event_type=event_type)
        if events.exists():  # Перевірка наявності подій
            event_data = []
            for event in events:
                player_name = f"{event.player.name} {event.player.surname}"
                minute = event.minute
                event_data.append({"player_name": player_name, "minute": minute})
            event_types[event_type_label] = event_data

    return event_types
