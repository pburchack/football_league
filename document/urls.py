# tournaments/urls.py

from django.urls import path
from .views import create_tournament, create_team, create_player

urlpatterns = [
    path('create_tournament/', create_tournament, name='create_tournament'),
    path('create_team/', create_team, name='create_team'),
    path('create_player/', create_player, name='create_player'),
]
