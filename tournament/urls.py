# tournament\urls.py
from django.urls import path
from . import views
from .views import PlayersCreateView, PlayersStats


app_name = "tournament"

urlpatterns = [
    path("", views.index, name="index"),
    path("login", views.login, name="login"),
    path("create_tournament/", views.create_tournament, name="create_tournament"),
    path("add_results/", views.add_results, name="add_results"),
    path("add_players/", PlayersCreateView.as_view(), name="create_player_form"),
    path("add_teams/", views.add_teams, name="add_teams"),
    path("add_add_match_results/", views.add_match_results, name="add_match_results"),
    path("view_results/", views.view_results, name="view_results"),
    path("players_statistics/", PlayersStats.as_view(), name="player_statistics"),
    path("team_statistics/", views.team_statistics, name="team_statistics"),
    path("about_site/", views.about, name="about_site"),
    path("some_player/<int:player_id>", views.show_player, name="show_player"),
]
