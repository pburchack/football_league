# tournament\vews.py
from os import name
from typing import Any
from django.http import HttpResponse
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views import generic
from tournament.models import Player, Team, Match, Tournament, PlayerEvent, PlayerStats
from django.views.generic import ListView


def login(request):
    return render(request, "tournament/login.html")


def index(request):
    return render(request, "tournament/index.html")


def create_tournament(request):
    return render(request, "tournament/create_tournament.html")


def add_results(request):
    return render(request, "tournament/add_results.html")


def view_results(request):
    num_match = Match.objects.count()
    tournaments = Tournament.objects.all()
    Tour_match_list = []

    for tournament in tournaments:
        matches = Match.objects.filter(tournament=tournament)
        matches_list = [match for match in matches]
        tour_match = {
            "tournament_name": tournament.name,
            "matches_list": matches_list,
        }
        Tour_match_list.append(tour_match)

    context = {
        "title": "Результати матчів",
        "num_match": num_match,
        "tour_match_list": Tour_match_list,
        "match_list": matches_list,
    }
    return render(request, "tournament/view_result.html", context=context)


class PlayersStats(ListView):
    model = Player
    template_name = "tournament/player_statistics.html"
    ordering = "-date_of_birth"
    context_object_name = "players"

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = "Статистика гравців"
        return context

    # метод для визначення даних які передаються в context_object_name
    def get_queryset(self):
        return Player.objects.filter(is_active=True)


class PlayersCreateView(generic.CreateView):
    model = Player
    fields = [
        "name",
        "surname",
        "date_of_birth",
        "is_active",
    ]
    success_url = reverse_lazy("tournament:player_statistics")
    template_name = "tournament/create_player_form.html"


def add_teams(request):
    pass


def add_match_results(request):
    pass


def show_player(request, player_id):
    return HttpResponse(f"Статистика гравця з id={player_id}")


def team_statistics(request):
    num_teams = Team.objects.count()
    teams = Team.objects.all()
    Team_players_list = []

    for team in teams:
        players = Player.objects.filter(team=team)
        players_list = [player for player in players]
        team_context = {
            "team_name": team.name,
            "players_list": players_list,
        }
        Team_players_list.append(team_context)

    context = {
        "title": "Статистика команд",
        "num_teams": num_teams,
        "team_players_list": Team_players_list,
    }
    return render(request, "tournament/team_statistics.html", context=context)


def about(request):
    return render(request, "tournament/about.html")
